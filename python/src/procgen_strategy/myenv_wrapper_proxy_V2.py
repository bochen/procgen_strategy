import numpy as np

import zmq 
from  procgen_strategy.myenv_wrapper import MyProcgenCEnvWrapper
import pickle
import uuid
from cprocgen_strategy import PyEpisode, PyGame, PyLevel
import json
import os
import datetime
from multiprocessing import Queue
from threading import Thread

sub_server_addr = 'tcp://127.0.0.1:12579'
pub_server_addr = 'tcp://127.0.0.1:12581'


def overrides(interface_class):

    def overrider(method):
        assert(method.__name__ in dir(interface_class))
        return method

    return overrider


# Define a function for the thread
def sub_handler(env, socket):
    while True:
        #  Wait for next request from client
        message = socket.recv_pyobj()
        if isinstance(message, tuple):
            env.queue.put(message)  
        else:
            print("unknown: " + str(message))

    
class MyProcgenEnvProxyWrapper(MyProcgenCEnvWrapper):
    """
    Procgen Wrapper file
    """

    def __init__(self, config, capacity=201):
        self.queue = Queue()
        self.name = "envclient_" + uuid.uuid4().hex
        self.file_index = 0
        
        self.n_level_update = 0
        self.n_episode_update = 0
        
        self.context = zmq.Context()
        self.subscriber = self.context.socket(zmq.SUB)
        self.subscriber.connect (pub_server_addr)     
        # Subscribes to all topics
        self.subscriber.subscribe("")

        self.publisher = self.context.socket(zmq.PUB)
        self.publisher.connect (sub_server_addr)
        
        thread = Thread(target=sub_handler, args=(self, self.subscriber))
        thread.daemon = True
        thread.start()

        super(MyProcgenEnvProxyWrapper, self).__init__(config, capacity)   

    def process_queue(self):
        while not(self.queue.empty()):
            abc = self.queue.get()
            source, name = abc[:2]
            if source == self.name: continue 
            
            if name == 'level':
                _, _, levelid, init_state = abc
                if not self.experience.contains(levelid):
                    self.experience.createOrGetLevel(levelid, init_state)
                    self.n_level_update += 1;
            elif name == 'episode':
                _, _, levelid, j = abc
                ep:PyEpisode = PyEpisode.newEpisode()
                ep.fromBinary(j)
                b = self.experience.onEpisodeDone(levelid, ep)
                if b:
                    self.n_episode_update += 1;
                else:
                    # print("ignored.......",levelid)
                    pass 
                
                if self.n_episode_update % 200 ==0:
                    print(self.summary())
            else:
                pass

    def summary(self):
        d = self.experience.summary()
        d.update({
            'self': self.name,
            'pid': os.getpid(),
            "n_level_update":self.n_level_update,
            "n_episode_update":self.n_episode_update,
            "queue_size": self.queue.qsize(),
            })
        return d
            
    @overrides(MyProcgenCEnvWrapper)     
    def on_eposide_finished(self, level, eposide):
        a = (self.name, 'episode', level.getId(), eposide.toBinary())
        self.publisher.send_pyobj(a)
        self.process_queue()
        
    @overrides(MyProcgenCEnvWrapper)     
    def on_newlevel_created(self, levelid, init_state):
        a = (self.name, "level", levelid, init_state)
        self.publisher.send_pyobj(a)
        self.process_queue()
            
    @overrides(MyProcgenCEnvWrapper)
    def close(self):
        print("closing...", flush=True)
        super(MyProcgenEnvProxyWrapper, self).close()
        # self._send_eposides()
            

if __name__ == "__main__":
    env_name = 'bigfish'
    env_name = 'coinrun'
    env = MyProcgenEnvProxyWrapper({'rand_seed':22, 'env_name':env_name}, capacity=10)
    print(env.observation_space)
    print(env.action_space)
    n = env.action_space.n
    ob = env.reset()
    print(ob.shape)
    for i in range(100000):
        observation, reward, done, info = env.step(np.random.randint(n))
        # observation, reward, done, info = env.step(0)
        if(i % 800 == 0): 
            #print (info, env.experience.summary())
            pass 
        if done:
            #print("Done ", i, reward, info['level_seed'])
            # break
            pass 

    print (env.summary())