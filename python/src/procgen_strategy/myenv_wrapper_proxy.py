import numpy as np

from ray.tune import registry

import zmq 
from  procgen_strategy.myenv_wrapper import MyProcgenCEnvWrapper
import pickle
import uuid
from cprocgen_strategy import PyEpisode, PyGame, PyLevel
import json
import os
import datetime


def overrides(interface_class):

    def overrider(method):
        assert(method.__name__ in dir(interface_class))
        return method

    return overrider

    
class MyProcgenEnvProxyWrapper(MyProcgenCEnvWrapper):
    """
    Procgen Wrapper file
    """

    def __init__(self, config, capacity=300, server_addr='tcp://127.0.0.1:12579'):
        self.server_addr = server_addr
        self.eposides = []
        self.name = "envclient_" + uuid.uuid4().hex
        self.file_index = 0
        
        self.context = zmq.Context()
        print ("Connecting to server...", self.server_addr, flush=True)
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect (self.server_addr)     

        super(MyProcgenEnvProxyWrapper, self).__init__(config, capacity)   

    @overrides(MyProcgenCEnvWrapper)     
    def on_eposide_finished(self, level, eposide):
        a = ('episode', level.getId(), eposide.toBinary())
        self.socket.send_pyobj(a)
        self.socket.recv_string()
        
    @overrides(MyProcgenCEnvWrapper)     
    def on_newlevel_created(self, levelid, init_state):
        a = ("level", levelid, init_state)
        self.socket.send_pyobj(a)
        self.socket.recv_string()        
            
    @overrides(MyProcgenCEnvWrapper)
    def _make_experience(self):
        self.experience = super(MyProcgenEnvProxyWrapper, self)._make_experience()
        self.socket.send_pyobj("REQ_MEMORY")
        path = self.socket.recv_string()
        self.experience.read(path)
        print("[{}] process {} load {} levels from {}".format(datetime.datetime.now(),
                                                              os.getpid(), self.experience.size(), path), flush=True)
        print(self.experience.summary(), flush=True)
        return self.experience
    
    @overrides(MyProcgenCEnvWrapper)
    def close(self):
        print("closing...", flush=True)
        super(MyProcgenEnvProxyWrapper, self).close()
        # self._send_eposides()
            

if __name__ == "__main__":
    env_name = 'bigfish'
    env_name = 'coinrun'
    env = MyProcgenEnvProxyWrapper({'rand_seed':22, 'env_name':env_name}, capacity=10)
    print(env.observation_space)
    print(env.action_space)
    n = env.action_space.n
    ob = env.reset()
    print(ob.shape)
    for i in range(100000):
        observation, reward, done, info = env.step(np.random.randint(n))
        # observation, reward, done, info = env.step(0)
        if(i % 800 == 0): 
            print (info, env.experience.summary())
        if done:
            print("Done ", i, reward, info['level_seed'])
            # break

