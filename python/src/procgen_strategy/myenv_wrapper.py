import time
import gym
import numpy as np
import pickle 

from ray.tune import registry
from procgen.env import ENV_NAMES as VALID_ENV_NAMES
from cprocgen_strategy import PyEpisode, PyGame, PyLevel
import ctypes
import hashlib

    
class MyProcgenCEnvWrapper(gym.Env):
    """
    Procgen Wrapper file
    """

    def __init__(self, config, capacity=300):
        self._default_config = {
            "num_levels" : 0,  # The number of unique levels that can be generated. Set to 0 to use unlimited levels.
            "env_name" : "coinrun",  # Name of environment, or comma-separate list of environment names to instantiate as each env in the VecEnv
            "start_level" : 0,  # The lowest seed that will be used to generated levels. 'start_level' and 'num_levels' fully specify the set of possible levels
            "paint_vel_info" : False,  # Paint player velocity info in the top left corner. Only supported by certain games.
            "use_generated_assets" : False,  # Use randomly generated assets in place of human designed assets
            "center_agent" : True,  # Determines whether observations are centered on the agent or display the full level. Override at your own risk.
            "use_sequential_levels" : False,  # When you reach the end of a level, the episode is ended and a new level is selected. If use_sequential_levels is set to True, reaching the end of a level does not end the episode, and the seed for the new level is derived from the current level seed. If you combine this with start_level=<some seed> and num_levels=1, you can have a single linear series of levels similar to a gym-retro or ALE game.
            "distribution_mode" : "easy",  # What variant of the levels to use, the options are "easy", "hard", "extreme", "memory", "exploration". All games support "easy" and "hard", while other options are game-specific. The default is "hard". Switching to "easy" will reduce the number of timesteps required to solve each game and is useful for testing or when working with limited compute resources. NOTE : During the evaluation phase (rollout), this will always be overriden to "easy"
        }
        self.config = self._default_config
        self.config.update(config)
        self.env_name = self.config.pop("env_name")
        assert self.env_name in VALID_ENV_NAMES
        self.capacity = capacity

        env = gym.make(f"procgen:procgen-{self.env_name}-v0", **self.config)
        self.env = env
        self.raw_env = env.env.env
        # Enable video recording features
        self.metadata = self.env.metadata

        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space
        self._done = True
        self._is_replaying = False
        self.last_state = None
        self.level:PyLevel = None 
        self.episode:PyEpisode = None 
        self.experience:PyGame = self._make_experience()
        
    def _make_experience(self):
        return PyGame(self.env_name, capacity=self.capacity)
        
    def _make_new_level(self, obs, state):
        if 0:
            levelid = np.sum(obs)
        else:
            levelid = hashlib.sha256(obs).hexdigest()
        b_exists = self.experience.contains(levelid)
        self.level = self.experience.createOrGetLevel(levelid, state)
        self.episode = self.level.newEpisode()
        if not b_exists:
            self.on_newlevel_created(levelid, state)
    
    def _make_rand_level(self):
        if  self.experience.needNewLevel():
            self._is_replaying = False 
            self.level = None 
            self.episode = None
        else:
            self._is_replaying = True
            self.level = self.experience.randGetLevel()
            self.episode = self.level.randEpisode()            
            
    def _reset(self):
        assert self._done, "procgen envs cannot be early-restarted"
        self._done = False         
        self._make_rand_level()
        level = self.level
        if level is None:  # create a new level
            if  self.last_state is None:
                obs = self.env.reset()
            else:
                self.raw_env.set_state(self.last_state)
                _rew, obs, _first = self.raw_env.observe()
                obs = obs['rgb'][0]
                
            self._make_new_level(obs=obs, state=self.raw_env.get_state())
            return obs, 0, False, {}
        else:  # instead of reset the it we randomly set it 
            self.raw_env.set_state(level.init_state)
            _rew, obs, _first = self.raw_env.observe()
            obs = obs['rgb'][0]
            for act in self.episode.actions:  # let skip the first few actions that are randomly chopped 
                obs, _rew, done, _info = self.env.step(act)
                assert not done  # should not be done if the replay is right
            return obs, 0, False, {}

    def reset(self):
        obs = self._reset()[0]
        return obs    

    def step(self, action):
        
        if self._done:
            self._reset()
        
        obs, rew, done, info = self.env.step(action)
        self._done = done
        
        if self._done and info['prev_level_complete']:
            self.episode.update(action, rew, True)
        else:
            self.episode.update(action, rew, False)

        # record last state
        if self._done:
            if not self._is_replaying:
                self.last_state = self.raw_env.get_state()

        if self._done:               
            self.experience.onEpisodeDone(self.level.getId(), self.episode)
            self.on_eposide_finished(self.level, self.episode)
        if self._done:
            rew += self.level.extraReward(self.episode)    
        else:
            #rew = 0
            pass

        if self._done:               
            self.level = None;self.episode = None  # for safe
        
        return obs, rew, done, info

    def on_eposide_finished(self, level, eposide):
        pass
    
    def on_newlevel_created(self, levelid, init_state):
        pass

    def render(self, mode="human"):
        return self.env.render(mode=mode)

    def close(self):
        return self.env.close()

    def seed(self, seed=None):
        return self.env.seed(seed)

    def __repr__(self):
        return self.env.__repr()

    @property
    def spec(self):
        return self.env.spec


# Register Env in Ray
registry.register_env(
    "myprocgen_cenv_wrapper",
    lambda config: MyProcgenCEnvWrapper(config)
)


def _test_main():
    env_name = 'bigfish'
    env_name = 'coinrun'
    env = MyProcgenCEnvWrapper({'rand_seed':22, 'env_name':env_name}, capacity=10)
    print(env.observation_space)
    print(env.action_space)
    n = env.action_space.n
    ob = env.reset()
    print(ob.shape)
    for i in range(100000):
        observation, reward, done, info = env.step(np.random.randint(n))
        if(i % 800 == 0): 
            print (info, env.experience.summary())
        if done:
            print("Done ", i, reward, info['level_seed'])
            # break

    
if __name__ == "__main__":
    _test_main()
