

import zmq 
import pickle

import os
from threading import Thread
from multiprocessing import Queue
import sys
from builtins import isinstance
import time
from queue import Empty
from datetime import datetime
from cprocgen_strategy import PyLevel, PyEpisode, PyGame

global_experience_file = ""
server_addr = 'tcp://127.0.0.1:12579'


# Define a function for the thread
def request_handler(server):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(server_addr)
    print("server binding at " + server_addr, flush=True)
    SUB_MEMORY = "SUB_MEMORY "
    while True:
        #  Wait for next request from client
        message = socket.recv_pyobj()
        if isinstance(message, tuple):
            socket.send_string("OK")
            server.queue.put(message)  
        elif message == "REQ_MEMORY":
            global global_experience_file
            socket.send_string(global_experience_file)
            server.n_client += 1
#         elif message.startswith(SUB_MEMORY):
#             socket.send_string("OK")
#             s = message[len(SUB_MEMORY):]
#             server.queue.put(s)
        elif message == "SUMMARY":            
            s = str(server.summary())
            socket.send_string(s)
        else:
            print("unknown: " + str(message))
    

class MyProcgenEnvServerWrapper:

    def __init__(self, envname, capacity=200):
        self.experience_prefix = "/tmp/myenv_wrapper_memory_" + envname
        os.system("rm -fr " + self.experience_prefix + "*")
        self.queue = Queue()

        self.experience = PyGame(envname, capacity + 1)
        
        self.n_update = 0
        self.n_level_update = 0
        self.n_episode_update = 0
        self.file_index = 0
        self.n_client = 0;
        self.write_time = 0
        
        self.write_experience()
        
    def summary(self):
        d = self.experience.summary()
        d.update({
            "n_level_update":self.n_level_update,
            "n_episode_update":self.n_episode_update,
            "queue_size": self.queue.qsize(),
            'global_experience_file': global_experience_file,
            })
        return d

    def write_experience(self):
        filepath = "{}_{}.exp".format(self.experience_prefix, self.file_index)
        print("[{}] Writing {} updates to ".format(datetime.now(), self.n_update) + filepath, flush=True)
        print("EXPSUMMARY: " + str(self.summary()), flush=True)
        self.experience.write(filepath)
        self.file_index = (self.file_index + 1) % 10
        global global_experience_file
        global_experience_file = filepath
        self.write_time = datetime.now()
    
    def start(self):
        thread = Thread(target=request_handler, args=(self,))
        thread.daemon = True
        thread.start()
        print("waiting requests", flush=True)
        while True:
            try:
                obj = self.queue.get(block=False)
            except Empty as _e :
                delta = datetime.now() - self.write_time
                if(delta.total_seconds() > 30 and self.n_update > 0):
                    self.write_experience()
                    self.n_update = 0
                elif(self.n_update > 100):
                    self.write_experience()
                    self.n_update = 0
                else:
                    time.sleep(2)
                continue
                    
            # print("processing", path, flush=True)
            
            try:
                self.processing(obj)
            except:
                print ("Unexpected error:", sys.exc_info()[0], flush=True)

    def processing(self, abc):
        name = abc[0]
        if name == 'level':
            _, levelid, init_state = abc
            if not self.experience.contains(levelid):
                self.experience.createOrGetLevel(levelid, init_state)
                self.n_update += 1
                self.n_level_update += 1;
        elif name == 'episode':
            _, levelid, j = abc
            ep:PyEpisode = PyEpisode.newEpisode()
            ep.fromBinary(j)
            b = self.experience.onEpisodeDone(levelid, ep)
            if b:
                self.n_update += 1
                self.n_episode_update += 1;
            else:
                # print("ignored.......",levelid)
                pass 
        else:
            print("unkown " + name, flush=True)
        if(self.n_update > 100):
            self.write_experience()
            self.n_update = 0

                    
if __name__ == "__main__":
    envname = 'coinrun'
    capacity = 200
    if len(sys.argv) >= 2: envname = sys.argv[1]
    if len(sys.argv) == 3: capacity = int(sys.argv[2])
    env = MyProcgenEnvServerWrapper(envname, capacity=capacity)
    if False:
        env.experience.createOrGetLevel(1212, b'BBBB')
        print(env.experience.size())
        env.write_experience()
        print(env.experience.size())
        newenv = PyGame("A", 1)
        print("load from " + global_experience_file)
        newenv.read(global_experience_file)
        print(newenv.size())
        
    env.start()
