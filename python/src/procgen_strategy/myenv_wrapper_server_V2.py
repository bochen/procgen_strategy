
import zmq 

sub_server_addr = 'tcp://127.0.0.1:12579'
pub_server_addr = 'tcp://127.0.0.1:12581'

class MyProcgenEnvServerWrapper:

    def __init__(self):
        pass 
    
    def start(self):
        context = zmq.Context()
        frontend = context.socket(zmq.XSUB)
        frontend.bind(sub_server_addr)
        backend  = context.socket(zmq.XPUB)
        backend.bind(pub_server_addr)
        zmq.proxy(frontend, backend, None)
        

                    
if __name__ == "__main__":
    env = MyProcgenEnvServerWrapper()
       
    env.start()
