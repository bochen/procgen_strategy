# distutils: language = c++

# cython: c_string_type=str, c_string_encoding=ascii

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.pair cimport pair  
from libcpp.map cimport map
from libcpp.memory cimport shared_ptr
from libc.stdint cimport uint32_t, uint64_t, uint8_t

ctypedef map[float, string] floatStringMap

cdef extern from "pylib.h" namespace "procgen_strategy":
    cdef cppclass GameCommon:
        GameCommon();
        float max_score;
        float min_score;
        float idle_score;

    cdef cppclass Episode:
        Episode();
        uint64_t getId();
        bool isReplay();
        bool isFinished();
        void update(uint8_t action, float reward, bool is_finished);
        float getCumReward();
        const vector[uint8_t]& getActions();
        string toBinary();
        void fromBinary(const string);
        string to_json_string();
        size_t length();
        

    
    cdef cppclass Level:
        shared_ptr[Episode] newEpisode();
        shared_ptr[Episode] randEpisode(uint32_t n_step_to_end); 
        string getId();
        string& getInitState();
        void update(shared_ptr[Episode] ep);
        float extraReward(shared_ptr[Episode] ep);

        
    cdef cppclass Game:
        Game(string name, uint32_t capacity);
        shared_ptr[Level] createOrGetLevel(string& id, string& init_state);
        shared_ptr[Level] randGetLevel();
        uint64_t size();
        string summary();
        bool onEpisodeDone(string levelid, shared_ptr[Episode] episode);
        bool needNewLevel();
        bool contains(string levelid);
        void write(string path);
        void read(string path);
        uint32_t MAX_EPISODE_LENGTH();
        
        
        