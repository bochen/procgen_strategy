# distutils: language = c++

# cython: c_string_type=str, c_string_encoding=ascii

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map
from libcpp.pair cimport pair 
from libcpp.memory cimport shared_ptr, allocator
from libcpp.utility cimport move
from cython.operator cimport dereference as deref
from libc.stdint cimport uint32_t, uint64_t

from cprocgen_strategy.strategy cimport GameCommon, Episode, Game, Level
import json
import pickle
import ctypes

ctypedef pair[int, int] mypair
ctypedef map[float, string] floatStringMap
ctypedef vector[string] StringVector

cdef class PyGameCommon:
    cdef GameCommon c_gamecommon;
    
    def __cinit__(self):
        self.c_gamecommon = GameCommon()
        
    @property
    def max_score(self):
        return self.c_gamecommon.max_score;

    @max_score.setter
    def max_score(self, x):
        self.c_gamecommon.max_score = x
        
    @property
    def min_score(self):
        return self.c_gamecommon.min_score;
    
    @property
    def idle_score(self):
        return self.c_gamecommon.idle_score;    
        
cdef class PyEpisode:
    cdef shared_ptr[Episode] cobj;

    def __cinit__(self):
        #raise Exception("NA")
        pass  
            
    def getId(self):
        return deref(self.cobj).getId();
    
    def length(self):
        return deref(self.cobj).length();    

    def isReplay(self):
        return deref(self.cobj).isReplay();    
    
    def isFinished(self):
        return deref(self.cobj).isFinished();    

    def update(self, action, rew, is_finished):
        return deref(self.cobj).update(action, rew, is_finished)
    
    cpdef toBinary(self):
        cdef bytes b = deref(self.cobj).toBinary()
        return b
    
    def fromBinary(self, s):
        deref(self.cobj).fromBinary(s)
    
    def to_json_string(self):
        return deref(self.cobj).to_json_string()


    @property
    def cum_reward(self):
        return deref(self.cobj).getCumReward();
    
    @staticmethod
    def newEpisode():
        return PyEpisode.c_newEpisode()

    @staticmethod 
    cdef c_newEpisode():
        cdef PyEpisode pc = PyEpisode()
        pc.cobj.reset(new Episode())
        return pc

    @property 
    def actions(self):
        return self.c_actions()
        
    cdef c_actions(self):
        cdef list s = deref(self.cobj).getActions();
        return s
    
cdef class PyLevel:
    cdef shared_ptr[Level] cobj;

    def __cinit__(self):
        pass  
    
    cpdef string getId(self):
        return deref(self.cobj).getId()

    @property 
    def init_state(self):
        return self.c_init_state()
    
    cdef c_init_state(self):
        cdef bytes s = deref(self.cobj).getInitState();
        return pickle.loads(s) 

    cpdef update(self, PyEpisode ep):
        deref(self.cobj).update(ep.cobj)

    cpdef extraReward(self,PyEpisode ep):
        deref(self.cobj).extraReward(ep.cobj)

        
    cpdef newEpisode(self):
        cdef PyEpisode pc = PyEpisode.c_newEpisode()
        pc.cobj = move(deref(self.cobj).newEpisode())
        return pc
    
    cpdef randEpisode(self, uint32_t n_step_to_end=0):
        cdef PyEpisode pc = PyEpisode.c_newEpisode()
        pc.cobj = move(deref(self.cobj).randEpisode(n_step_to_end))
        return pc
    
cdef class PyGame:
    cdef shared_ptr[Game] cobj;

    def __cinit__(self, name, capacity):
        self.cobj.reset(new Game(name, capacity))

    def size(self):
        return deref(self.cobj).size();

    @property
    def MAX_EPISODE_LENGTH(self):
        return deref(self.cobj).MAX_EPISODE_LENGTH();
        
    def summary(self):
        s = deref(self.cobj).summary();
        return json.loads(s);
    
    cpdef randGetLevel(self):
        cdef PyLevel pc = PyLevel()
        pc.cobj = move(deref(self.cobj).randGetLevel())
        return pc
    
    def needNewLevel(self):
        return deref(self.cobj).needNewLevel();
    
    def contains(self,levelid):
        return deref(self.cobj).contains(levelid);    

    def write(self, path):
        return deref(self.cobj).write(path)
    
    def read(self, path):
        return deref(self.cobj).read(path)
            
    cpdef onEpisodeDone(self, string levelid, PyEpisode episode):
        return deref(self.cobj).onEpisodeDone(levelid, episode.cobj)
    
    cpdef createOrGetLevel(self, string id, init_state):
        cdef PyLevel pc = PyLevel()
        s = pickle.dumps(init_state)
        pc.cobj = move(deref(self.cobj).createOrGetLevel(id, s))
        return pc
    
