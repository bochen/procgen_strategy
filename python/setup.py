# from distutils.core import setup
# from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from setuptools import setup, find_packages, Extension, Command
import os 

DIR = os.path.dirname(os.path.realpath(__file__))


def abs_path(path):
    #return os.path.join(DIR,path)
    return path;

ext_modules = [
    Extension("cprocgen_strategy",
              [abs_path("src/cprocgen_strategy/strategy.pyx"),
               abs_path("../src/common.cpp"),
               abs_path("../src/Episode.cpp"),
               ],
              language="c++",
              extra_compile_args=['-std=c++11',  '-fopenmp', '-I'+ abs_path("../src")],
              extra_link_args=[ '-fopenmp'],
              libraries=[]),
    ]

setup(
    name='pyprogen_strategy',
    version='0.2',
    description='Python lib for procgen_strategy',
    author='Bo Chen',
    author_email='bochen0909@gmail.com',
    packages=['procgen_strategy'],
    package_dir={'procgen_strategy': abs_path('src/procgen_strategy')},
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules,
    test_suite="test",
    include_package_data = True,
    package_data={'': []},

)
