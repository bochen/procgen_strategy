/*
 * Episode.h
 *
 *  Created on: Aug 16, 2020
 *      Author: bo
 */

#ifndef EPISODE_H_
#define EPISODE_H_

#include <vector>
#include <memory>
#include <stdint.h>
#include <atomic>
#include <sstream>

#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>

#include "json.hpp"

#include "common.h"

using json = nlohmann::json;

namespace procgen_strategy {

class Episode;
class Level;
class Game;
typedef std::shared_ptr<Episode> Episode_ptr;

typedef struct {
	float cum_reward;
	bool is_finish;
	size_t length;

	template<class Archive>
	void serialize(Archive &archive) {
		archive(cum_reward, is_finish, length);
	}
	json to_json() {
		json j;
		j["cum_reward"] = cum_reward;
		j["is_finish"] = is_finish;
		j["length"] = length;
		return j;
	}

} ParentInfo;

class Episode {

public:
	friend class Level;
	friend class Game;
	Episode() :
			id(rand()) {
	}
	virtual ~Episode() {
	}
public:
	uint64_t id;

	std::vector<uint8_t> actions;
	std::vector<float> rewards;
	float cum_reward = 0;
	bool is_finish = false;
	std::shared_ptr<ParentInfo> parent_info = NULL;

public:

	template<class Archive>
	void serialize(Archive &archive) {
		archive(id, cum_reward, is_finish, actions, rewards, parent_info);
	}

	bool isFinished() {
		return is_finish;
	}

	std::string to_json_string() {
		return to_json().dump();
	}

	json to_json() {
		json j;
		j["id"] = id;
		j["cum_reward"] = cum_reward;
		j["is_finish"] = is_finish;
		json j_action(actions);
		j["actions"] = j_action;
		json j_rewards(rewards);
		j["rewards"] = j_rewards;
		if (parent_info) {
			j["parent_info"] = parent_info->to_json();
		}
		return j;
	}

	std::string toBinary() {
		std::ostringstream os;
		cereal::BinaryOutputArchive oarchive(os);
		oarchive(*this);
		return os.str();
	}

	void fromBinary(const std::string &s) {
		auto thisid = id;
		std::istringstream os(s);
		cereal::BinaryInputArchive iarchive(os);
		iarchive(*this);
		this->id = thisid;
	}

	float getScore() {
		return cum_reward;
	}
	float getCumReward() {
		return cum_reward;
	}

	const std::vector<uint8_t>& getActions() {
		return actions;
	}

	uint64_t getId() {
		return id;
	}

	bool isReplay() {
		return parent_info.get() != NULL;
	}

	size_t length() {
		return actions.size();
	}
	void update(uint8_t act, float rew, bool is_finish) {
		actions.push_back(act);
		rewards.push_back(rew);
		this->is_finish = is_finish;
		cum_reward += rew;
	}

	Episode_ptr rand_copy(uint32_t max_episode_length, uint32_t n_step_to_end =
			0) {
		Episode *ep = new Episode();
		if (length() == 0) {
			return Episode_ptr(ep);
		} else {
			ep->parent_info.reset(new ParentInfo( { cum_reward, is_finish,
					length() }));
			if (n_step_to_end == 0) {
				if (length() < max_episode_length && !is_finish) {
					if (rand01() > 0.5) {
						n_step_to_end = length() - 20;
						if (n_step_to_end < 0) {
							n_step_to_end = length();
						}
					} else {
						n_step_to_end = int(length() * 0.2);
						if (n_step_to_end < 0) {
							n_step_to_end = length();
						}
					}
				} else {
					n_step_to_end = length();
				}
			} else if (n_step_to_end > length()) {
				n_step_to_end = length();
			}
			auto min = length() - n_step_to_end;
			auto max = length() - 3;
			if (max > min) {
				uint32_t idx = rand_int(min, max);
				for (uint32_t i = 0; i <= idx; i++) {
					(*ep).update(actions.at(i), rewards.at(i), false);
				}
			}
			return Episode_ptr(ep);
		}

	}

	float extra_bonus(const GameCommon &gc) {
		if (parent_info) {

			auto &pi = *parent_info;
			if (cum_reward == 0 && pi.cum_reward == 0) { //both don't have reward
				if (length() == pi.length) {
					return 0;
				} else if (length() > pi.length) { //more time means more explore
					return std::abs(gc.ewmean_score) * 0.1;
				} else {
					return -std::abs(gc.ewmean_score) * 0.1;;
				}
			} else { // either has reward
				if (is_finish and pi.is_finish) { //both finish
					if (length() == pi.length) { //equal time
						return 0;
					} else if (length() > pi.length) { //use more to finish
						return -std::abs(cum_reward) * 0.2;
					} else { //use less to finish
						return std::abs(cum_reward) * 0.2;
					}
				} else if (is_finish and !pi.is_finish) { //finish!
					return std::abs(cum_reward) * 0.2;
				} else if (!is_finish and pi.is_finish) { //can finish but not
					return -std::abs(cum_reward) * 0.2;
				} else { //neither finished
					if (cum_reward > pi.cum_reward) { //more reward
						return std::abs(cum_reward) * 0.2;
					} else if (cum_reward < pi.cum_reward) { //less reward
						return -std::abs(cum_reward) * 0.2;
					} else { //same reward
						if (length() < pi.length) { //use less time
							return std::abs(cum_reward) * 0.2;
						} else { //if use same or more time, say it could have done better
							return -std::abs(cum_reward) * 0.2;
						}
					}
				}
			}
		} else { //no opinion
			return 0;
		}
	}

};

} //end procgen_strategy

#endif /* EPISODE_H_ */
