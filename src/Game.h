/*
 * game.h
 *
 *  Created on: Aug 16, 2020
 *      Author: bo
 */

#ifndef GAME_H_
#define GAME_H_

#include <string>
#include <unordered_map>
#include <memory>
#include <iostream>
#include <fstream>
#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>

#include "common.h"
#include "Level.h"
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

namespace procgen_strategy {

typedef std::shared_ptr<Level> Level_ptr;

class Game {

public:
	Game(std::string name, uint32_t capacity) :
			name(name), capacity(capacity) {

	}

	Level_ptr randGetLevel0() {
		assert(!levels.empty());

		int k = rand_int(0, size());
		int i = 0;
		std::unordered_map<std::string, Level_ptr>::iterator itor =
				levels.begin();
		for (; itor != levels.end(); itor++) {
			if (i++ == k) {
				return itor->second;
			}
		}
		assert(false); //never be here;
		return itor->second;
	}

	Level_ptr randGetLevel() {
		float min_score = game_common.max_score * 0.00001f;
		std::vector<std::string> keys;
		std::vector<float> scores;
		for (auto &kv : levels) {
			keys.push_back(kv.first);
			scores.push_back(
					std::max(min_score,
							game_common.max_score - kv.second->ewmean_score));
		}

		size_t idx = rand_discrete(scores);
		return levels.at(keys[idx]);
	}

	Level_ptr createOrGetLevel(const std::string &id,
			const std::string &init_state) {
		if (contains(id)) {
			return levels.at(id); //when #level is limited, this may happen;
		} else {
			_purge();
			auto level = Level_ptr(new Level(id, init_state, &game_common));
			levels[id] = level;
			return levels.at(id);
		}
	}

	std::string getMaxScoreLevelItor() {
		assert(size() > 0);
		float max_score = std::numeric_limits<float>::min();
		std::string maxid;
		for (std::unordered_map<std::string, Level_ptr>::iterator itor =
				levels.begin(); itor != levels.end(); itor++) {
			auto s = itor->second->maxScore();
			if (s > max_score) {
				maxid = itor->first;
			}
		}
		return maxid;
	}

	void _purge() {
		if (false) {
			while (size() > capacity) {
				auto itor = getMaxScoreLevelItor();
				levels.erase(itor);
			}
		} else if (false) {
			while (size() > capacity) {
				levels.erase(++levels.begin());
			}
		} else {
			while (size() > capacity) {
				auto i = rand_int(0, levels.size());
				size_t j = 0;
				for (std::unordered_map<std::string, Level_ptr>::iterator itor =
						levels.begin(); itor != levels.end(); itor++, j++) {
					if (i == j) {
						levels.erase(itor);
						break;
					}
				}
			}
		}
	}

	uint64_t size() {
		return levels.size();
	}

	uint32_t MAX_EPISODE_LENGTH() {
		return game_common.max_length;
	}

	bool contains(const std::string &levelid) {
		return levels.find(levelid) != levels.end();
	}

	bool onEpisodeDone(const std::string &levelid, Episode_ptr episode) {

		if (contains(levelid)) {
			levels.at(levelid)->update(episode);
			return true;
		} else {
			return false;
		}
	}

	std::pair<double, double> finishedRatio() {
		if (size() == 0) {
			return std::make_pair(0.0, 0.0);
		}
		int n = 0;
		double score = 1e-8;
		for (std::unordered_map<std::string, Level_ptr>::iterator itor =
				levels.begin(); itor != levels.end(); itor++) {
			if (itor->second->isFinished()) {
				n++;
			}
			score += itor->second->ewmean_score;
		}
		return std::make_pair((double) n / size(), score / size());
	}

	bool needNewLevel() {
		if ((double) size() / capacity <= 0.20) {
			return true;
		}
		auto fr = finishedRatio();
		double p = (fr.first + fr.second / game_common.max_score) / 2;
		return rand01() < p * p;
	}

	template<class Archive>
	void serialize(Archive &archive) {
		archive(name, capacity, game_common, levels);
	}

	void write(std::string path) {
		std::ofstream os(path);
		cereal::BinaryOutputArchive oarchive(os);
		oarchive(*this);
	}

	void read(std::string path) {
		std::ifstream os(path);
		cereal::BinaryInputArchive iarchive(os);
		iarchive(*this);
		for (std::unordered_map<std::string, Level_ptr>::iterator itor =
				levels.begin(); itor != levels.end(); itor++) {
			itor->second->setGameCommon(&game_common);
		}
	}

	std::string summary() {
		json j;
		j["global_common"] = game_common.to_json();
		j["#level"] = size();
		j["capacity"] = capacity;
		auto fr = finishedRatio();
		j["%finished"] = fr.first * 100;
		j["level_mean_ews"] = fr.second;
		j["lmews/ms"] = (double) fr.second / game_common.max_score;
		j["name"] = name;
		return j.dump();
	}

protected:
	string name;
	uint32_t capacity;
	GameCommon game_common;
	std::unordered_map<std::string, Level_ptr> levels;

};

} // end namespace procgen_strategy

#endif /* GAME_H_ */
