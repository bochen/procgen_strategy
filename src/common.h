/*
 * common.h
 *
 *  Created on: Aug 16, 2020
 *      Author: bo
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <vector>
#include <stdlib.h>
#include "json.hpp"

using json = nlohmann::json;

namespace procgen_strategy {

typedef struct {
	float max_score = 1;
	float min_score = 0;
	float idle_score = 0;
	float ewmean_score=0;
	int max_length=1000;

	json to_json() {
		json j;
		j["max_score"] = max_score;
		j["min_score"] = min_score;
		j["idle_score"] = idle_score;
		j["max_length"] = max_length;
		j["ewmean_score"] = ewmean_score;
		return j;
	}
	template<class Archive>
	void serialize(Archive &archive) {
		archive(min_score, max_score, idle_score, ewmean_score, max_length);
	}
} GameCommon;

float rand01();

uint32_t rand_int(uint32_t min, uint32_t max);

size_t rand_discrete(const std::vector<float> &scores);

} // end namespace procgen_strategy

#endif /* COMMON_H_ */
