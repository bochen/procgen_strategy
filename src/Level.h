/*
 * Level.h
 *
 *  Created on: Aug 16, 2020
 *      Author: bo
 */

#ifndef LEVEL_H_
#define LEVEL_H_

#include <vector>
#include <unordered_map>
#include <limits>
#include <iostream>
#include <algorithm>
#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include "common.h"
#include "Episode.h"
namespace procgen_strategy {

class Level {
	friend class Game;
public:
	Level() :
			game_common(0), is_finished(false), capacity(0), max_score(-99999), id(
					""), ewmean_score(0) {

	}

	Level(const std::string &id, const std::string &init_state,
			GameCommon *game_common, size_t capacity = 64) :
			id(id), init_state(init_state), game_common(game_common), capacity(
					capacity), max_score(-99999), is_finished(false), ewmean_score(
					0) {
	}
public:
	virtual ~Level() {
	}

	const std::string& getId() {
		return id;
	}

	uint64_t getMaxScoreEpisodeItor() {
		assert(!episodes.empty());
		float max_score = std::numeric_limits<float>::min();
		uint64_t maxid = -1;
		for (std::unordered_map<uint64_t, Episode_ptr>::iterator itor =
				episodes.begin(); itor != episodes.end(); itor++) {
			auto s = itor->second->getScore();
			if (s > max_score) {
				maxid = itor->first;
			}
		}
		return maxid;
	}

	float maxScore() {
		return max_score;
	}

	size_t size() {
		return episodes.size();
	}
	void _purge() {
		if (false) {
			while (size() > capacity) {
				auto itor = getMaxScoreEpisodeItor();
				episodes.erase(itor);

			}
		} else {
			while (size() > capacity) {
				episodes.erase(episodes.begin());
			}
		}
	}

	bool isFinished() {
		return is_finished;
	}

	void update(Episode_ptr ep) {
		episodes[ep->getId()] = ep; //since every ep is unique, just add new or overide it.

		game_common->ewmean_score = game_common->ewmean_score * 0.98
				+ 0.02 * ep->cum_reward;
		ewmean_score = ewmean_score * 0.95 + 0.05 * ep->cum_reward;
		if (ep->is_finish) {
			this->is_finished = true;
		}
		if (ep->cum_reward > max_score) {
			max_score = ep->cum_reward;
		}
		if (ep->cum_reward > game_common->max_score) {
			game_common->max_score = ep->cum_reward;
		}
		if (ep->cum_reward < game_common->min_score) {
			game_common->min_score = ep->cum_reward;
		}
		if (ep->length() > game_common->max_length) {
			game_common->max_length = ep->length();
		}
		_purge();
	}

	Episode_ptr randEpisode(uint32_t n_step_to_end) {
		_purge();
		if (size() == 0) {
			return newEpisode();
		}
		float min_score = game_common->max_score * 0.00001f;
		double threshold = this->ewmean_score / max_score;
		if (rand01() < threshold) { //worse cases
			std::vector<uint64_t> keys;
			std::vector<float> scores;
			for (auto &kv : episodes) {
				keys.push_back(kv.first);
				scores.push_back(
						std::max(min_score,
								game_common->max_score
										- kv.second->getScore()));
			}

			size_t idx = rand_discrete(scores);
			return episodes[keys[idx]]->rand_copy(game_common->max_length,
					n_step_to_end);
		} else {
			std::vector<uint64_t> keys;
			std::vector<float> scores;
			for (auto &kv : episodes) {
				keys.push_back(kv.first);
				scores.push_back(std::max(min_score, kv.second->getScore()));
			}

			size_t idx = rand_discrete(scores);
			return episodes[keys[idx]]->rand_copy(game_common->max_length,
					n_step_to_end);

		}
	}

	uint64_t getMaxEposideId() {
		uint64_t i = 0;
		for (auto &kv : episodes) {
			if (i < kv.second->id) {
				i = kv.second->id;
			}
		}
		return i;
	}

	float extraReward(Episode_ptr &ep) {
		return ep->extra_bonus(*game_common);
	}

	Episode_ptr newEpisode() {
		_purge();
		Episode_ptr ep(new Episode());
		episodes[ep->getId()] = ep;
		return ep;
	}

	const std::string& getInitState() {
		return init_state;
	}

	void setGameCommon(GameCommon *gc) {
		game_common = gc;
	}

	template<class Archive>
	void serialize(Archive &archive) {
		archive(id, init_state, capacity, max_score, ewmean_score, is_finished,
				episodes);
	}
protected:
	std::string id;
	std::string init_state;
	size_t capacity;
	GameCommon *game_common;

	float max_score;
	float ewmean_score;
	bool is_finished;

	std::unordered_map<uint64_t, Episode_ptr> episodes;

};

} //end namespace
#endif /* LEVEL_H_ */
