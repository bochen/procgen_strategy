/*
 * Episode.cpp
 *
 *  Created on: Aug 16, 2020
 *      Author: bo
 */

#include "Episode.h"

namespace procgen_strategy {

float rand01() {
	return (double) rand() / ((double) RAND_MAX + 1);
}

uint32_t rand_int(uint32_t min, uint32_t max) {
	if (max > min) {
		return min + (rand() % static_cast<uint32_t>(max - min));
	} else {
		return 0;
	}

}

size_t rand_discrete(const std::vector<float> &scores) {
	// 1 2 3 4 5 -> 1 3 6 10 15
	float cum[scores.size()];
	float sum = 0;
	for (size_t i = 0; i < scores.size(); i++) {
		sum += scores.at(i);
		cum[i] = sum;
	}

	float f = rand01() * sum;
	for (size_t i = 0; i < scores.size(); i++) {
		if (f < cum[i]) {
			return i;
		}
	}
	return scores.size() - 1; //rare
}

}
