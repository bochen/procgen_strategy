#include "acutest.h"

#include <exception>
#include <stdexcept>
#include <string>
#include <algorithm>
#include "Episode.h"

using namespace std;
using namespace procgen_strategy;

#define TEST_INT_EQUAL(a,b) \
		TEST_CHECK( (a) == (b)); \
		TEST_MSG("Expected: %d", (a)); \
		TEST_MSG("Produced: %d", (b));

#define TEST_DOUBLE_EQUAL(a,b) \
		TEST_CHECK( std::abs((a) - (b))<1e-10); \
		TEST_MSG("Expected: %f", (double)(a)); \
		TEST_MSG("Produced: %f", (double)(b));

#define TEST_STR_EQUAL(a,b) \
		TEST_CHECK( (a) == (b)); \
		TEST_MSG("Expected: %s", (a)); \
		TEST_MSG("Produced: %s", (b).c_str());

void test_to_json1(void) {
	Episode ep1;
	ep1.is_finish=true;
	ep1.parent_info.reset(new ParentInfo( { 1,true,12}));
	cout << ep1.to_json_string() << endl;
	Episode ep2;
	ep2.fromBinary(ep1.toBinary());
	cout << ep2.to_json_string() << endl;
}

void test_to_json2(void) {
	Episode ep1;
	ep1.update(12,2.2,false);
	ep1.update(12,2.2,false);
	ep1.update(12,2.2,false);
	ep1.update(12,2.2,true);
	cout << ep1.to_json_string() << endl;
	Episode ep2;
	ep2.fromBinary(ep1.toBinary());
	assert(ep2.parent_info.get()==NULL);
	cout << ep2.to_json_string() << endl;
}

TEST_LIST = { {"test_to_json1", test_to_json1},
		{"test_to_json2", test_to_json2},
	{	NULL, NULL}};

